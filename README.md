# TWAK10 Container Lab

Containerlabb för kursen [Trender i IT-infrastruktur och nätverksdesign](https://ju.se/studera/valj-utbildning/kurser.html?courseCode=TWAK10&semester=20212&revision=2,000&lang=sv) som en del av [IT-infrastruktur och nätverksdesign](https://ju.se/dit) programmet vid [Jönköping University](https://ju.se).

----

Laborationen är uppdelad över flera branches. Clona eller ladda ned den branch som tillhör den del av laborationen du är vid.
